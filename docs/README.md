Develop simple blog using react & redux.

### Features
* List of posts
* Details of post
* About us page
* Contacts page
* Widget of categories
* Widget of Tags
* Search
* Sign in (by email)
* Sign up (by email)
* Leave comments as a user
* 404 page
* 403 page


### Requirements
* API could be Firebase, NodeJS, RoR.
* Any type of database (RDS or NoSQL).
* Authentication: HTTP Bearer or JSON Web Token.

### Recommended packages
* sass
* autoprefixer
* prettier
* eslint
* axios
* lodash
* create-react-app
* prop-types
* classnames
* react-intl
* react-router
* redux
* redux-form
* redux-thunk
* enzyme

